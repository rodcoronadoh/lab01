package com.example.rodrigo.test;

/**
 * Created by Rodrigo on 3/10/2015.
 */
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import com.example.rodrigo.data.Contract;
import com.example.rodrigo.data.DbHelper;

public class TestDb extends AndroidTestCase {
    public static final String LOG_TAG = TestDb.class.getSimpleName();
    public void testCreateDb() throws Throwable {
        mContext.deleteDatabase(DbHelper.DATABASE_NAME);
        SQLiteDatabase db = new DbHelper(
                this.mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }
    public void testInsertReadDb() {
// Test data we're going to insert into the DB to see if it works.
        String testLocationSetting = "22355659";
        String testCityName = "North Pole";
        String testLatitude = "Lunes a viernes";
        String testLongitude = "www.google.com";
// If there's an error in those massive SQL table creation Strings,
// errors will be thrown here when you try to get a writable database.
       DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
// Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(Contract.ContractEntry.COLUMN_DIR, testLocationSetting);
        values.put(Contract.ContractEntry.COLUMN_TEL, testCityName);
        values.put(Contract.ContractEntry.COLUMN_HORARIO, testLatitude);
        values.put(Contract.ContractEntry.COLUMN_WEB, testLongitude);
        long locationRowId;
        locationRowId = db.insert(Contract.ContractEntry.TABLE_NAME, null, values);
// Verify we got a row back.
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);
// Data's inserted. IN THEORY. Now pull some out to stare at it and verify it made
// the round trip.
// Specify which columns you want.
        String[] columns = {
                Contract.ContractEntry._ID,
                Contract.ContractEntry.COLUMN_DIR,
                Contract.ContractEntry.COLUMN_TEL,
                Contract.ContractEntry.COLUMN_HORARIO,
                Contract.ContractEntry.COLUMN_WEB
        };
// A cursor is your primary interface to the query results.
        Cursor cursor = db.query(
               Contract.ContractEntry.TABLE_NAME, // Table to Query
                columns,
                null, // Columns for the "where" clause
                null, // Values for the "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null // sort order
        );
// If possible, move to the first row of the query results.
        if (cursor.moveToFirst()) {
// Get the value in each column by finding the appropriate column index.
            int locationIndex = cursor.getColumnIndex( Contract.ContractEntry.COLUMN_DIR);
            String location = cursor.getString(locationIndex);
            int nameIndex = cursor.getColumnIndex(( Contract.ContractEntry.COLUMN_TEL));
            String name = cursor.getString(nameIndex);
            int latIndex = cursor.getColumnIndex(( Contract.ContractEntry.COLUMN_HORARIO));
            double latitude = cursor.getDouble(latIndex);
            int longIndex = cursor.getColumnIndex( Contract.ContractEntry.COLUMN_WEB);
            double longitude = cursor.getDouble(longIndex);
// Hooray, data was returned! Assert that it's the right data, and that the database
// creation code is working as intended.
// Then take a break. We both know that wasn't easy.
            assertEquals(testCityName, name);
            assertEquals(testLocationSetting, location);
            assertEquals(testLatitude, latitude);
            assertEquals(testLongitude, longitude);
// Fantastic. Now that we have a location, add some weather!
        } else {
// That's weird, it works on MY machine...
            fail("No values returned :(");
        }
// Fantastic. Now that we have a location, add some weather!
        ContentValues weatherValues = new ContentValues();
        weatherValues.put( Contract.ContractEntry.COLUMN_LOC_KEY, locationRowId);
        weatherValues.put( Contract.ContractEntry.COLUMN_DIR, "guATEMALA ZONA 15");
        weatherValues.put( Contract.ContractEntry.COLUMN_TEL, "23692045");
        weatherValues.put( Contract.ContractEntry.COLUMN_HORARIO, "lUNES A VIERNES");
        weatherValues.put( Contract.ContractEntry.COLUMN_WEB, "www.google.com");


        dbHelper.close();
    }
}