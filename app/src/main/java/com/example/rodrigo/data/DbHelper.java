package com.example.rodrigo.data;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Manages a local database for weather data.
 */
public class DbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "datos.db";
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {



        final String SQL_CREATE_Photos_TABLE = "CREATE TABLE " + Contract.ContractEntry.ContractPhotos.TABLE_NAME + " (" +
                Contract.ContractEntry.ContractPhotos._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                Contract.ContractEntry.ContractPhotos.COLUMN_DES + " TEXT UNIQUE NOT NULL, " +
                Contract.ContractEntry.ContractPhotos.COLUMN_LISTC + " TEXT NOT NULL, " +
                Contract.ContractEntry.ContractPhotos.COLUMN_NUMF + " TEXT NOT NULL,";


        final String SQL_CREATE_DATOS_TABLE = "CREATE TABLE " + Contract.ContractEntry.TABLE_NAME + " (" +

                Contract.ContractEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                Contract.ContractEntry.COLUMN_LOC_KEY + " INTEGER NOT NULL, " +
                Contract.ContractEntry.COLUMN_DIR + " TEXT NOT NULL, " +
                Contract.ContractEntry.COLUMN_TEL + " TEXT NOT NULL, " +
                Contract.ContractEntry.COLUMN_HORARIO + " TEXT NOT NULL," +
                Contract.ContractEntry.COLUMN_WEB + " TEXT NOT NULL, "+
                " FOREIGN KEY (" + Contract.ContractEntry.COLUMN_LOC_KEY + ") REFERENCES " +
                Contract.ContractEntry.TABLE_NAME + " (" + Contract.ContractEntry.ContractPhotos._ID + "), " +

                " UNIQUE (" + Contract.ContractEntry.ContractPhotos.COLUMN_ID + ", " +
                Contract.ContractEntry.COLUMN_LOC_KEY + ") ON CONFLICT REPLACE);";




        sqLiteDatabase.execSQL(SQL_CREATE_DATOS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_Photos_TABLE);


    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Contract.ContractEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Contract.ContractEntry.ContractPhotos.TABLE_NAME);
        onCreate(sqLiteDatabase);

    }
}