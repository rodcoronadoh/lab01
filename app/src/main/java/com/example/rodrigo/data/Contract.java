package com.example.rodrigo.data;

import android.provider.BaseColumns;

/**
 * Created by Rodrigo on 3/10/2015.
 */
public class    Contract {


    public static final class ContractEntry implements BaseColumns {
        public static final String TABLE_NAME = "datos";

        public static final String COLUMN_LOC_KEY = "name_id";

        public static final String COLUMN_DIR = "dirreccion";

        public static final String COLUMN_TEL = "tel";

        public static final String COLUMN_HORARIO = "horario";

        public static final String COLUMN_WEB = "web";


        //photo class
        public static final class ContractPhotos implements BaseColumns {
            public static final String TABLE_NAME = "photodb";

            public static final String COLUMN_ID = "name_id";

            public static final String COLUMN_DES = "descripcion";

            public static final String COLUMN_LISTC = "listadoComentarios";

            public static final String COLUMN_NUMF = "numFavoritos";
        }

    }
}
